package com.manabreak.ld35;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

public class FinalStage extends Stage {
    private final GameScreen screen;


    public FinalStage(GameScreen screen) {
        super(new ExtendViewport(Gdx.graphics.getBackBufferWidth() / 3, Gdx.graphics.getBackBufferHeight() / 3));
        this.screen = screen;

        Label.LabelStyle style = new Label.LabelStyle(Res.font, Color.WHITE);
        Label label = new Label("Congratulations!", style);
        label.setPosition(getWidth() / 2f - label.getWidth() / 2f, getHeight() / 2f - label.getHeight() / 2f + 35f);
        addActor(label);
        label.setColor(1f, 1f, 1f, 0f);
        label.addAction(Actions.fadeIn(3f));

        Label l2 = new Label("And BIG THANKS for playing!", style);
        l2.setPosition(getWidth() / 2f - l2.getWidth() / 2f, getHeight() / 2f - label.getHeight() / 2f);
        addActor(l2);
        l2.setColor(1f, 1f, 1f, 0f);
        l2.addAction(Actions.sequence(Actions.delay(2f), Actions.fadeIn(3f)));

        Label l3 = new Label("See ya in LD36!", style);
        l3.setPosition(getWidth() / 2f - l3.getWidth() / 2f, getHeight() / 2f - label.getHeight() / 2f - 35f);
        addActor(l3);
        l3.setColor(1f, 1f, 1f, 0f);
        l3.addAction(Actions.sequence(Actions.delay(4f), Actions.fadeIn(3f)));
    }
}
