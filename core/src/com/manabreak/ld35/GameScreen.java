package com.manabreak.ld35;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class GameScreen implements Screen {
    private final LdGame game;
    private GameStage gameStage;
    private Gui gui;
    private boolean shouldReset = false;
    private boolean shouldShowCongrats = false;
    private Stage finalStage;

    public GameScreen(LdGame game) {
        this.game = game;
        this.gameStage = new GameStage(this);
        this.gui = new Gui(this);
        this.finalStage = new FinalStage(this);
    }

    public void reset() {
        shouldReset = true;
    }

    private void resetInternal() {
        shouldReset = false;
        this.gameStage.cleanUp();
        Key.KEYS_COUNT = 0;
        this.gameStage = new GameStage(this);
        this.gui = new Gui(this);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClearColor(0f, 0f, 0f, 1.f);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        if (shouldReset) {
            resetInternal();
        }

        if (!shouldShowCongrats) {
            gameStage.act(delta);
            gameStage.draw();
            gui.act(delta);
            gui.draw();
        } else {
            finalStage.act(delta);
            finalStage.draw();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public LdGame getGame() {
        return game;
    }

    public GameStage getGameStage() {
        return gameStage;
    }

    public Gui getGui() {
        return gui;
    }

    public void showCongratulations() {
        shouldShowCongrats = true;
    }
}
