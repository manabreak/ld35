package com.manabreak.ld35;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.scenes.scene2d.Group;

public class Player extends Group implements PhysicsBody<Player> {

    public static final String TAG = "Player";

    private static final float FLICKER_SPEED = 0.06f;
    private static final float BAT_HORIZONTAL_POWER = 4f;
    private static final float BAT_MAX_VEL_X = 9f;
    private static final float BAT_FLAP_POWER = 6f;
    private static final float BAT_MAX_VEL_Y = 10f;
    private static final float MOUSE_HORIZONTAL_POWER = 4f;
    private static final float MOUSE_MAX_VEL_X = 9f;

    private final GameStage stage;
    private Entity bat;
    private Entity mouse;
    private Body body;

    private TextureRegion[] batFrames;
    private TextureRegion[] mouseFrames;

    private float frameTimer = 0f;
    private float batFrameSpeed = 0.3f;
    private int frame = 0;
    private boolean damaged = false;
    private float damageTimer = 0f;
    private boolean shouldDraw = true;
    private float drawTimer = 0f;
    private boolean lastRight = true;

    private boolean isMouse = false;

    BodyDef bodyDef;
    private float mouseFrameSpeed = 0.1f;
    private boolean canJump = false;
    private boolean jumping = false;

    public Player(GameStage stage) {
        this.stage = stage;
        bat = new Entity(stage, Res.createSprite("bat_0"));
        bat.setSize(32f, 32f);
        batFrames = new TextureRegion[]{Res.findRegion("bat_0"), Res.findRegion("bat_1")};

        mouse = new Entity(stage, Res.createSprite("mouse_0"));
        mouse.setSize(18f, 16f);
        mouseFrames = new TextureRegion[]{Res.findRegion("mouse_0"), Res.findRegion("mouse_1"), Res.findRegion("mouse_2")};

        addActor(bat);
        // addActor(mouse);

        bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.fixedRotation = true;

        createBatPhysics();
        body.setUserData(this);
    }

    public float getShapeX() {
        return getX() + (isMouse ? 9f : 16f);
    }

    public float getShapeY() {
        return getY() + (isMouse ? 8f : 16f);
    }

    public void changeShape() {
        frameTimer = 0f;
        Vector2 pos = body.getPosition();
        Vector2 vel = body.getLinearVelocity();
        stage.getWorld().destroyBody(body);
        if (isMouse) {
            mouse.remove();
            addActor(bat);
            createBatPhysics();
            isMouse = false;
        } else {
            bat.remove();
            addActor(mouse);
            createMousePhysics();
            isMouse = true;
        }
        body.setLinearVelocity(vel);
        body.setTransform(pos, 0f);
        body.setUserData(this);


    }

    private void createMousePhysics() {
        body = stage.getWorld().createBody(bodyDef);
        /*
        CircleShape shape = new CircleShape();
        shape.setRadius(0.25f);
        shape.setPosition(new Vector2(-0.3f, -0.2f));
        FixtureDef fd = new FixtureDef();
        fd.shape = shape;

        Fixture f = body.createFixture(fd);
        shape.setPosition(new Vector2(0.3f, -0.2f));
        fd = new FixtureDef();
        fd.shape = shape;
        body.createFixture(fd);
        */

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(0.5f, 0.125f, new Vector2(0f, -0.4f), 0f);
        FixtureDef fd = new FixtureDef();
        fd.shape = shape;
        body.createFixture(fd);
        shape.dispose();
    }

    private void createBatPhysics() {
        body = stage.getWorld().createBody(bodyDef);
        CircleShape shape = new CircleShape();
        shape.setRadius(0.52f);
        FixtureDef fd = new FixtureDef();
        fd.shape = shape;
        Fixture f = body.createFixture(fd);
        shape.dispose();
    }

    @Override
    public void setPosition(float x, float y) {
        body.setTransform(x * C.SCR2BOX, y * C.SCR2BOX, 0f);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (shouldDraw) {
            mouse.sprite.setFlip(!lastRight, false);
            bat.sprite.setFlip(!lastRight, false);
            super.draw(batch, parentAlpha);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (damaged) {
            drawTimer += delta;
            if (drawTimer >= FLICKER_SPEED) {
                drawTimer -= FLICKER_SPEED;
                shouldDraw = !shouldDraw;
            }
            damageTimer += delta;
            if (damageTimer >= 0.3f) {
                damaged = false;
                shouldDraw = true;
            }
        }

        float ax = isMouse ? 9f : 16f;
        float ay = isMouse ? 8f : 16f;

        super.setPosition(body.getPosition().x * C.BOX2SCR - ax, body.getPosition().y * C.BOX2SCR - ay);
        updateAnimation(delta);

        if (isMouse) {
            handleMouseInput();
        } else {
            handleBatInput();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.X)) {
            changeShape();
        }
    }

    private void handleMouseInput() {
        if (!damaged) {
            float vx = 0f;

            if (canJump && Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
                Res.jump.play(0.8f);
                canJump = false;
                frame = 2;
                body.applyLinearImpulse(0f, 6f, 0f, 0f, true);
            }

            if (!huggingRight && Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                vx = MOUSE_HORIZONTAL_POWER;
                lastRight = true;
            }
            if (!huggingLeft && Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                vx = -MOUSE_HORIZONTAL_POWER;
                lastRight = false;
            }

            if (vx != 0f) {
                body.applyLinearImpulse(vx, 0f, 0f, 0f, true);
                if (Math.abs(body.getLinearVelocity().x) > MOUSE_MAX_VEL_X) {
                    body.setLinearVelocity(body.getLinearVelocity().x < 0f ? -MOUSE_MAX_VEL_X : MOUSE_MAX_VEL_X, body.getLinearVelocity().y);
                }
            } else {
                vx = body.getLinearVelocity().x;
                vx *= 0.85f;
                body.setLinearVelocity(vx, body.getLinearVelocity().y);
            }
        }
    }

    private void handleBatInput() {
        if (!damaged) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
                Res.flap.play(0.65f);
                if (body.getLinearVelocity().y < 0f) {
                    body.setLinearVelocity(body.getLinearVelocity().x, body.getLinearVelocity().y * 0.2f);
                }
                body.applyLinearImpulse(0f, BAT_FLAP_POWER, 0f, 0f, true);
                if (body.getLinearVelocity().y > BAT_MAX_VEL_Y) {
                    body.setLinearVelocity(body.getLinearVelocity().x, BAT_MAX_VEL_Y);
                }
                flapping = true;
                bat.sprite.setRegion(batFrames[0]);
                frameTimer = 0f;
                canJump = false;
            }

            float vx = 0f;
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                vx = BAT_HORIZONTAL_POWER;
                lastRight = true;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                vx = -BAT_HORIZONTAL_POWER;
                lastRight = false;
            }

            if (vx != 0f) {
                body.applyLinearImpulse(vx, 0f, 0f, 0f, true);
                if (Math.abs(body.getLinearVelocity().x) > BAT_MAX_VEL_X) {
                    body.setLinearVelocity(body.getLinearVelocity().x < 0f ? -BAT_MAX_VEL_X : BAT_MAX_VEL_X, body.getLinearVelocity().y);
                }
            } else {
                vx = body.getLinearVelocity().x;
                vx *= 0.5f;
                body.setLinearVelocity(vx, body.getLinearVelocity().y);
            }
        }
    }

    private boolean flapping = false;

    private void updateAnimation(float delta) {
        frameTimer += delta;
        if (isMouse && canJump) {
            if (Math.abs(body.getLinearVelocity().x) > 0f) {
                if (frameTimer >= mouseFrameSpeed) {
                    frameTimer -= mouseFrameSpeed;
                    frame++;
                    frame %= mouseFrames.length;
                    mouse.sprite.setRegion(mouseFrames[frame]);
                }
            } else {
                mouse.sprite.setRegion(mouseFrames[0]);
                frameTimer = 0f;
            }
        } else {
            if (flapping) {
                frameTimer += delta;
                if (frameTimer >= 0.1f) {
                    bat.sprite.setRegion(batFrames[1]);
                }
                if (frameTimer >= 0.7f) {
                    flapping = false;
                    frame = 0;
                    frameTimer = 0;
                }
            } else {
                if (body.getLinearVelocity().y >= -0.05f) {
                    bat.sprite.setRegion(batFrames[1]);
                } else {
                    bat.sprite.setRegion(batFrames[0]);
                }
            }
        }
    }

    public float getBodyX() {
        return body.getPosition().x;
    }

    public float getBodyY() {
        return body.getPosition().y;
    }

    public void doDamage(Vector2 position) {
        Vector2 v = body.getPosition().sub(position);
        v = v.nor();
        v = v.scl(16f);
        body.applyLinearImpulse(v.x, v.y, 0f, 0f, true);
        damageTimer = 0f;
        drawTimer = 0f;
        damaged = true;
    }


    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public Player getObject() {
        return this;
    }

    int jCounter = 0;

    boolean huggingRight = false, huggingLeft = false;

    public void resetHugs() {
        huggingLeft = huggingRight = false;
    }

    @Override
    public void beginContact(Contact contact, PhysicsBody other) {

        boolean hitGround = false;
        int numContacts = contact.getWorldManifold().getNumberOfContactPoints();
        if (numContacts > 0) {
            Vector2[] points = contact.getWorldManifold().getPoints();
            Vector2 mid = points[0];
            if (numContacts > 1) {
                mid.x = (mid.x + points[1].x) / 2f;
                mid.y = (mid.y + points[1].y) / 2f;
            }

            if (mid.x > body.getPosition().x - 0.45f && mid.x < body.getPosition().x + 0.45f &&
                    mid.y < body.getPosition().y) {
                hitGround = true;
                frameTimer = 0f;
                System.out.println("Can jump again" + (jCounter++));
            } else if (mid.x > body.getPosition().x + 0.45f) {
                huggingRight = true;
            } else if (mid.x < body.getPosition().x - 0.45f) {
                huggingLeft = true;
            }
        }

        if (hitGround) {
            canJump = true;
            jumping = false;
        }

    }

    @Override
    public Body getBody() {
        return body;
    }

    public boolean isDamaged() {
        return damaged;
    }

    public void kill() {
        bat.remove();
        mouse.remove();
    }
}
