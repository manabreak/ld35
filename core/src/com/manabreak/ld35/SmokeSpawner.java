package com.manabreak.ld35;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.ArrayList;
import java.util.List;

public class SmokeSpawner {


    private class Smoke extends Entity {

        private float timer = 0f;
        private Vector2 vel = new Vector2();

        public Smoke(GameStage stage, Sprite sprite) {
            super(stage, sprite);
            setSize(smokeSize, smokeSize);
        }

        public void spawnAt(float x, float y) {
            setPosition(x - getWidth() / 2f, y - getHeight() / 2f);
            setScale(MathUtils.random(0.8f, 1f));
            timer = 0f;
            vel = vel.set(MathUtils.random(-1f, 1f), MathUtils.random(0.1f, 1f));
            vel = vel.nor().scl(MathUtils.random(5f, smokeSpeed));
        }

        @Override
        public void act(float delta) {
            super.act(delta);

            moveBy(vel.x * delta, vel.y * delta);
            setScale(1f - (timer / 4f));
            setAlpha(1f - (timer / 4f));

            timer += delta;
            if (timer >= 4f) {
                this.remove();
            }
        }
    }

    private final GameStage stage;

    private List<Smoke> smokes = new ArrayList<>();
    private int i = 0;
    private float smokeSize = 8f;
    private float smokeSpeed = 25f;

    public void setSize(float size) {
        smokeSize = size;
    }

    public void setSpeed(float speed) {
        smokeSpeed = speed;
    }

    public SmokeSpawner(GameStage stage) {
        this.stage = stage;

        for (int i = 0; i < 200; ++i) {
            smokes.add(new Smoke(stage, Res.createSprite("smoke")));
        }
    }

    public void spawnBefore(float x, float y, Actor a) {
        Smoke s = smokes.get(i);
        i++;
        i %= smokes.size();

        s.spawnAt(x, y);

        stage.getRoot().addActorBefore(a, s);
    }

    public void spawnAfter(float x, float y, Actor a) {
        Smoke s = smokes.get(i);
        i++;
        i %= smokes.size();

        s.spawnAt(x, y);

        stage.getRoot().addActorAfter(a, s);
    }
}
