package com.manabreak.ld35.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.manabreak.ld35.LdGame;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        config.title = "Key Keeper";
        config.width = 1080;
        config.height = 720;

        /*
        TexturePacker.Settings s = new TexturePacker.Settings();
        s.filterMag = Texture.TextureFilter.Nearest;
        s.filterMin = Texture.TextureFilter.Nearest;
        s.maxWidth = 1024;
        s.maxHeight = 1024;
        s.paddingX = 3;
        s.paddingY = 3;
        s.edgePadding = true;
        s.alias = false;
        s.bleed = true;
        s.duplicatePadding = true;
        s.useIndexes = false;

        TexturePacker.process(s, "../../images", "../../android/assets/graphics", "game");
        */

        new LwjglApplication(new LdGame(), config);
    }
}
