package com.manabreak.ld35;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import java.util.ArrayList;
import java.util.List;

public class GameStage extends Stage implements ContactListener {
    private final GameScreen gameScreen;

    private World world;
    private Box2DDebugRenderer debugRenderer;

    private Player player;
    private boolean debugDraw = false;
    private boolean renderLights = false;

    private int health = 6;

    private OrthogonalTiledMapRenderer mapRenderer;
    private TiledMap tiledMap;
    private List<Body> wallBodies = new ArrayList<Body>();
    private List<Body> spikeBodies = new ArrayList<Body>();

    private int[] foregroundLayers;
    private int[] backgroundLayers;

    private List<Ghost> ghosts = new ArrayList<>();
    private List<Key> keys = new ArrayList<>(7);
    private int keysCollected = 0;
    private int keysTotal = 7;
    private List<Body> bodiesToDestroy = new ArrayList<>(10);

    private SmokeSpawner smokeSpawner;
    private float gameOverTimer = 4f;
    private boolean gameOver = false;

    private Entity fade;
    private Sprite vignette;
    private boolean gameStarted = true;
    private boolean completed = false;

    public GameStage(GameScreen gameScreen) {
        super(new ExtendViewport(Gdx.graphics.getBackBufferWidth() / 3, Gdx.graphics.getBackBufferHeight() / 3));
        this.gameScreen = gameScreen;

        world = new World(new Vector2(0f, -10f), true);
        world.setContactListener(this);
        debugRenderer = new Box2DDebugRenderer();

        fade = new Entity(this, Res.createSprite("black"));
        fade.setSize(getWidth() * 2f, getHeight() * 2f);
        fade.setAlpha(1f);
        fade.addAction(Actions.fadeOut(4f));
        addActor(fade);

        vignette = Res.createSprite("vignette");
        vignette.setSize(getWidth(), getHeight());

        player = new Player(this);
        addActor(player);

        tiledMap = new TmxMapLoader().load("map0.tmx");
        mapRenderer = new OrthogonalTiledMapRenderer(tiledMap);

        smokeSpawner = new SmokeSpawner(this);

        createPhysicsWalls();
        createSpikes();
        splitLayers();
        handleSpawns();
    }

    private void handleSpawns() {
        MapLayer spawns = tiledMap.getLayers().get("Spawns");
        for (MapObject obj : spawns.getObjects()) {

            float px = (Float) obj.getProperties().get("x");
            float py = (Float) obj.getProperties().get("y");

            if (obj.getName().equals("Player")) {
                player.setPosition(px, py);
            } else if (obj.getName().equals("Ghost")) {
                Ghost g = new Ghost(this, obj);
                ghosts.add(g);
                addActor(g);
            } else if (obj.getName().equals("Key")) {
                Key k = new Key(this, obj);
                keys.add(k);
                addActor(k);
            } else if (obj.getName().equals("Door")) {
                MapProperties p = obj.getProperties();
                float x = (Float) p.get("x");
                float y = (Float) p.get("y");
                float w = (Float) p.get("width");
                float h = (Float) p.get("height");
                x *= C.SCR2BOX;
                y *= C.SCR2BOX;
                w *= C.SCR2BOX;
                h *= C.SCR2BOX;

                BodyDef bodyDef = new BodyDef();
                bodyDef.type = BodyDef.BodyType.StaticBody;
                final Body body = world.createBody(bodyDef);
                FixtureDef fixtureDef = new FixtureDef();
                PolygonShape shape = new PolygonShape();
                shape.setAsBox(w / 2f, h / 2f);
                fixtureDef.shape = shape;
                Fixture f = body.createFixture(fixtureDef);
                f.setSensor(true);
                body.setTransform(x + w / 2f, y + h / 2f, 0f);
                body.setUserData(new PhysicsBody<Object>() {
                    @Override
                    public String getTag() {
                        return "Door";
                    }

                    @Override
                    public Object getObject() {
                        return null;
                    }

                    @Override
                    public void beginContact(Contact contact, PhysicsBody other) {

                    }

                    @Override
                    public Body getBody() {
                        return body;
                    }
                });
                shape.dispose();
            }
        }
    }

    private void splitLayers() {
        List<Integer> fg = new ArrayList<Integer>();
        List<Integer> bg = new ArrayList<Integer>();

        MapLayers layers = tiledMap.getLayers();
        for (int i = 0; i < layers.getCount(); ++i) {
            if (layers.get(i).getName().startsWith("FG")) {
                if (layers.get(i).isVisible()) {
                    fg.add(i);
                }
            } else {
                if (layers.get(i).isVisible()) {
                    bg.add(i);
                }
            }
        }

        foregroundLayers = new int[fg.size()];
        for (int i = 0; i < fg.size(); ++i) foregroundLayers[i] = fg.get(i);
        backgroundLayers = new int[bg.size()];
        for (int i = 0; i < bg.size(); ++i) backgroundLayers[i] = bg.get(i);
    }

    private void createSpikes() {
        MapLayer spikesLayer = tiledMap.getLayers().get("Spikes");
        for (MapObject obj : spikesLayer.getObjects()) {
            MapProperties p = obj.getProperties();
            float x = (Float) p.get("x");
            float y = (Float) p.get("y");
            float w = (Float) p.get("width");
            float h = (Float) p.get("height");
            x *= C.SCR2BOX;
            y *= C.SCR2BOX;
            w *= C.SCR2BOX;
            h *= C.SCR2BOX;

            BodyDef bodyDef = new BodyDef();
            bodyDef.type = BodyDef.BodyType.StaticBody;
            final Body body = world.createBody(bodyDef);
            FixtureDef fixtureDef = new FixtureDef();
            PolygonShape shape = new PolygonShape();
            shape.setAsBox(w / 2f, h / 2f);
            fixtureDef.shape = shape;
            Fixture f = body.createFixture(fixtureDef);
            body.setTransform(x + w / 2f, y + h / 2f, 0f);
            body.setUserData(new PhysicsBody<Object>() {
                @Override
                public String getTag() {
                    return "Spike";
                }

                @Override
                public Object getObject() {
                    return null;
                }

                @Override
                public void beginContact(Contact contact, PhysicsBody other) {

                }

                @Override
                public Body getBody() {
                    return body;
                }
            });
            spikeBodies.add(body);
            shape.dispose();
        }

    }

    private void createPhysicsWalls() {
        MapLayers layers = tiledMap.getLayers();
        MapLayer physicsLayer = layers.get("Walls");
        for (MapObject mapObject : physicsLayer.getObjects()) {
            MapProperties properties = mapObject.getProperties();
            float x = (Float) properties.get("x");
            float y = (Float) properties.get("y");
            float w = (Float) properties.get("width");
            float h = (Float) properties.get("height");

            float r = (Float) properties.get("rotation", 0f, Float.class);

            x *= C.SCR2BOX;
            y *= C.SCR2BOX;
            w *= C.SCR2BOX;
            h *= C.SCR2BOX;

            BodyDef bodyDef = new BodyDef();
            bodyDef.type = BodyDef.BodyType.StaticBody;
            Body body = world.createBody(bodyDef);
            FixtureDef fixtureDef = new FixtureDef();
            PolygonShape shape = new PolygonShape();
            shape.setAsBox(w / 2f, h / 2f);
            fixtureDef.shape = shape;
            Fixture fixture = body.createFixture(fixtureDef);
            body.setTransform(x + w / 2f, y + h / 2f, 0f);
            wallBodies.add(body);
            // body.setUserData(m_wallPhysicsBody);
            shape.dispose();
        }
    }

    public GameScreen getGameScreen() {
        return gameScreen;
    }

    public World getWorld() {
        return world;
    }

    @Override
    public void act(float delta) {
        for (Body body : bodiesToDestroy) {
            world.destroyBody(body);
        }
        bodiesToDestroy.clear();

        if (gameStarted) {
            gameOverTimer -= delta;
            if (gameOverTimer < 0f) {
                gameStarted = false;
            }
        }

        super.act(delta);
        if (health > 0) {
            getCamera().position.x = player.getBodyX() * C.BOX2SCR;
            getCamera().position.y = player.getBodyY() * C.BOX2SCR;
            Res.setMusicVolume(Res.getMusicVolume() + (0.8f - Res.getMusicVolume()) * delta);
        } else {
            gameOverTimer += delta;
            Res.setMusicVolume(0.8f - 0.8f * (gameOverTimer / 4f));

            if (gameOverTimer >= 4f) {
                Res.setMusicVolume(0.8f);
                gameScreen.reset();
                health = 1;
            }
        }
        world.step(delta, 10, 10);


        if (Gdx.input.isKeyJustPressed(Input.Keys.F1)) {
            debugDraw = !debugDraw;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.F2)) {
            renderLights = !renderLights;
        }

        if (health > 0) {
            smokeSpawner.spawnBefore(player.getShapeX(), player.getShapeY(), player);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.F12)) {
            initGameOver();
        }
    }

    @Override
    public void draw() {

        if (gameOverTimer > 4f) return;

        mapRenderer.setView((OrthographicCamera) getCamera());
        mapRenderer.render(backgroundLayers);

        super.draw();

        mapRenderer.render(foregroundLayers);

        vignette.setPosition(getCamera().position.x - vignette.getWidth() / 2f, getCamera().position.y - vignette.getHeight() / 2f);
        getBatch().begin();
        vignette.draw(getBatch(), 1f);
        getBatch().end();

        if (fade.getAlpha() > 0f) {
            fade.setPosition(getCamera().position.x - fade.getWidth() / 2f, getCamera().position.y - fade.getHeight() / 2f);
            getBatch().begin();
            // fade.setAlpha(MathUtils.clamp(gameOverTimer / 4f, 0f, 1f));
            fade.draw(getBatch(), 1f);
            getBatch().end();
        }

        if (renderLights) {
            Matrix4 p = getCamera().projection.cpy();
            Matrix4 v = getCamera().view.cpy();
            v.scl(C.BOX2SCR);
            p.mul(v);
        }

        if (debugDraw) {
            Matrix4 m = getCamera().combined.cpy();
            m.scl(C.BOX2SCR);
            debugRenderer.render(world, m);
        }
    }

    public Player getPlayer() {
        return player;
    }

    private void handlePlayerContact(PhysicsBody other) {
        String tag = other.getTag();
        if (tag.equals(Ghost.TAG) || tag.equals("Spike")) {
            if (!player.isDamaged() && health > 0) {

                health--;
                System.out.println("Ouch! " + health + "/6");
                player.doDamage(other.getBody().getPosition());
                gameScreen.getGui().setHealth(health);
                if (health == 0) {
                    Res.death.play();
                    System.out.println("Game over!");
                    initGameOver();
                } else {
                    Res.hit.play();
                }
            }
        } else if (tag.equals(Key.TAG)) {
            Res.key.play();
            keysCollected++;
            Key key = (Key) other;
            key.remove();
            gameScreen.getGui().keyCollected(key);
            System.out.println("Key " + keysCollected + "/" + keysTotal + " collected!");
        } else if (tag.equals("Door")) {
            if (keysCollected == 7) {
                System.out.println("CONGRATULATIONS!");
                fade.addAction(
                        Actions.sequence(Actions.fadeIn(4f),
                                Actions.run(new Runnable() {
                                    @Override
                                    public void run() {
                                        gameScreen.showCongratulations();
                                    }
                                })));
                completed = true;
            } else {
                System.out.println("Still " + (7 - keysCollected) + " keys to go...");
            }
        }
    }

    private void initGameOver() {
        player.kill();
        fade.addAction(Actions.fadeIn(4f));
        gameOverTimer = 0f;
        gameOver = true;
    }

    @Override
    public void beginContact(Contact contact) {
        PhysicsBody o1 = (PhysicsBody) contact.getFixtureA().getBody().getUserData();
        PhysicsBody o2 = (PhysicsBody) contact.getFixtureB().getBody().getUserData();

        if (o1 != null && o2 != null) {
            o1.beginContact(contact, o2);
            String s1 = o1.getTag();
            String s2 = o2.getTag();
            if (s1.equals(Player.TAG)) {
                handlePlayerContact(o2);

            } else if (s2.equals(Player.TAG)) {
                handlePlayerContact(o1);
            }
        } else if (o1 != null) {
            o1.beginContact(contact, null);
        } else if (o2 != null) {
            o2.beginContact(contact, null);
        }
    }

    @Override
    public void endContact(Contact contact) {
        player.resetHugs();
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    public void queueForDestruction(Body body) {
        bodiesToDestroy.add(body);
    }

    public void cleanUp() {
        world.dispose();
    }
}
