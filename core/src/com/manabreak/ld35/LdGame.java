package com.manabreak.ld35;

import com.badlogic.gdx.Game;

public class LdGame extends Game {

    private GameScreen gameScreen;

    @Override
    public void create() {
        Res.load();
        gameScreen = new GameScreen(this);
        setScreen(gameScreen);
    }
}
