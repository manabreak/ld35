package com.manabreak.ld35;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class Key extends Entity implements PhysicsBody<Key> {
    public static final String TAG = "Key";

    public static int KEYS_COUNT = 0;

    private Body body;

    public Key(GameStage gameStage, MapObject obj) {
        super(gameStage, Res.createSprite("key_" + KEYS_COUNT++));

        float x = (Float) (obj.getProperties().get("x"));
        float y = (Float) (obj.getProperties().get("y"));
        float w = (Float) (obj.getProperties().get("width"));
        float h = (Float) (obj.getProperties().get("height"));

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        body = gameStage.getWorld().createBody(bodyDef);

        FixtureDef def = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(0.25f);
        def.shape = shape;
        Fixture f = body.createFixture(def);
        f.setSensor(true);
        shape.dispose();
        body.setUserData(this);

        setPosition(x, y);

        x *= C.SCR2BOX;
        y *= C.SCR2BOX;
        w *= C.SCR2BOX;
        h *= C.SCR2BOX;
        body.setTransform(x + w / 2f, y + h / 2f, 0f);

    }

    @Override
    public boolean remove() {
        gameStage.queueForDestruction(body);
        return super.remove();
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public Key getObject() {
        return this;
    }

    @Override
    public void beginContact(Contact contact, PhysicsBody other) {

    }

    @Override
    public Body getBody() {
        return body;
    }
}
